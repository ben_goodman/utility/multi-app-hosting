output "bucket_name" {
    value = module.website.bucket_name
}

output "cloudfront_id" {
  value = module.website.cloudfront_id
}

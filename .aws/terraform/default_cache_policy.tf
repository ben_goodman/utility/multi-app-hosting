resource "aws_cloudfront_cache_policy" "default" {
    name        = "default-cache-policy-${random_id.cd_function_suffix.hex}-${var.project_name}-${var.resource_namespace}"
    comment     = "${var.project_name}-${var.resource_namespace}"
    default_ttl = 3600
    max_ttl     = 86400
    min_ttl     = 0
    parameters_in_cache_key_and_forwarded_to_origin {
        cookies_config {
            cookie_behavior = "all"
        }
        headers_config {
            header_behavior = "none"
        }
        query_strings_config {
            query_string_behavior = "all"
        }
        enable_accept_encoding_brotli = true
        enable_accept_encoding_gzip = true
    }
}
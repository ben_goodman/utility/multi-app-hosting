resource "aws_iam_role" "lambda_role" {
  name = "lambda-role-multi-app-hosting"

  assume_role_policy = jsonencode({
    Version = "2012-10-17"
    Statement = [{
      Effect = "Allow"
      Principal = {
          Service: [
              "lambda.amazonaws.com",
          ]
      }
      Action = "sts:AssumeRole"
    }]
  })
}

resource "aws_iam_policy" "assets_bucket_access_policy" {
  name = "s3-access-policy-multi-app-hosting"

  policy = jsonencode({
    Version = "2012-10-17"
    Statement = [
      {
        Effect = "Allow"
        Action = [
            "s3:ListBucket",
            "s3:GetObject",
            "s3:PutObject",
        ]
        Resource = [
          "arn:aws:s3:::multi-app-hosting-default-dev.bgoodman",
          "arn:aws:s3:::multi-app-hosting-default-dev.bgoodman/*"
        ]
      }
    ]
  })
}

resource "aws_iam_role_policy_attachment" "assets_bucket_access_policy_attachment" {
  role       = aws_iam_role.lambda_role.name
  policy_arn = aws_iam_policy.assets_bucket_access_policy.arn
}


module "sitemap_generator_payload" {
    source  = "gitlab.com/ben_goodman/lambda-function/aws//modules/code_builder"
    version = "4.0.0"

    working_dir     = "${path.module}/sitemap_generator"
    command   = "npm  ci && npm run build"
    archive_source_file = "${path.module}/sitemap_generator/dist/index.js"
}

module "sitemap_generator_lambda_function" {
    source  = "gitlab.com/ben_goodman/lambda-function/aws"
    version = "4.0.0"

    function_name    = "${var.project_name}-sitemap-generator-${terraform.workspace}"
    lambda_payload   = module.sitemap_generator_payload.archive_file
    function_handler = "index.handler"
    memory_size      = 512
    role             = aws_iam_role.lambda_role
    runtime          = "nodejs18.x"

    environment_variables = {
        BUCKET_NAME = "multi-app-hosting-default-dev.bgoodman"
    }
}



resource "aws_lambda_permission" "allow_bucket" {
  statement_id  = "AllowExecutionFromS3Bucket"
  action        = "lambda:InvokeFunction"
  function_name = module.sitemap_generator_lambda_function.arn
  principal     = "s3.amazonaws.com"
  source_arn    = "arn:aws:s3:::multi-app-hosting-default-dev.bgoodman"
}

resource "aws_s3_bucket_notification" "bucket_notification" {
  bucket = "multi-app-hosting-default-dev.bgoodman"

  lambda_function {
    lambda_function_arn = module.sitemap_generator_lambda_function.arn
    events              = ["s3:ObjectCreated:*", "s3:ObjectRemoved:*"]
    filter_suffix       = ".js"
  }

  depends_on = [aws_lambda_permission.allow_bucket]
}
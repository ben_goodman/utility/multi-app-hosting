resource "aws_cloudfront_function" "viewer_request" {
    name    = "multiapp-viewer-request-${random_id.cd_function_suffix.hex}"
    comment = "Inserts document root into request URIs."
    runtime = "cloudfront-js-1.0"
    publish = true
    code    = file("${path.module}/cloudfront_functions/viewer_request.js")
}

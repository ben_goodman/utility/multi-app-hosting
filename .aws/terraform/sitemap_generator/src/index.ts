import type { S3Event } from 'aws-lambda'
import { PutObjectCommand, DeleteObjectCommand, S3Client } from '@aws-sdk/client-s3'
import { S3 } from 'aws-sdk'

const BUCKET_NAME = process.env.BUCKET_NAME!
const s3 = new S3Client({})

const getObjects = async (): Promise<string[]> => {
    const s3 = new S3()
    const objects = await s3.listObjectsV2({ Bucket: BUCKET_NAME }).promise()
    const objectKeys = objects.Contents?.map((object) => {
        return object.Key
    })

    return objectKeys
}

const generateIndexPage = (objects: string[]) => {
    const links = objects.map((object) => {
        const dirName = object.split('/')[0]
        return `<a class="default-link" href="${object}"><h2>${dirName}</h2></a>`
    })

    const html = `
    <html>
        <head>
            <title>Apps | ben.website</title>
        </head>
        <body>
            <style>
                #root {
                    display: flex;
                    flex-direction: column;
                }
                .default-link {
                    margin: 10px;
                }
                .og-link {
                    display: flex;
                    flex-direction: column;
                    margin: 10px;
                }
                img {
                    width: 50%;
                }
            </style>
            <div id="root">
                ${links.join('<br>')}
            </div>
            <script type="module">
                import { fetch as fetchOpengraph } from 'https://cdn.jsdelivr.net/npm/fetch-opengraph@1.0.36/+esm'
                const links = document.querySelectorAll('.default-link')
                links.forEach((link) => {
                    fetchOpengraph(link.href).then((data) => {
                        const title = data.ogTitle ? data.ogTitle : data.title
                        const description = data.ogDescription ? data.ogDescription : data.description
                        const image = data.ogImage ? data.ogImage.url : data.image
                        const url = data.ogUrl ? data.ogUrl : data.url
                        const html = \`
                        <a class="og-link" href=\${url}>
                            <h2>\${title}</h2>
                            <p>\${description}</p>
                            <img src="\${image}" />
                        </a>
                        \`
                        link.outerHTML = html
                    })
                })
            </script>
        </body>
    </html>
    `

    return html
}


export const handler = async (event: S3Event ) => {
    console.log('event', JSON.stringify(event))

    // if the event is for the index.html file, return
    if (event.Records[0].s3.object.key === 'index.html') {
        return
    }

    const objectKeys = await getObjects()

    // get keys ending in /index.html
    const indexObjects = objectKeys.filter((key) => key.endsWith('/index.html'))

    const indexHtml = generateIndexPage(indexObjects)

    // put the objects into an s3 object /index.html
    await s3.send(new PutObjectCommand({
        Bucket: BUCKET_NAME,
        Key: 'index.html',
        Body: indexHtml,
        ContentType: 'text/html',
        CacheControl: 'max-age=0', // set the cache control header
        // set the cache control header
    }))


}

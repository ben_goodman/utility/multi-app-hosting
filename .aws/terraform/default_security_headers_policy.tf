locals {
    csp_allowed_domains = join(" ", var.website_aliases, ["*.ben.website","localhost:1234", "cdn.jsdelivr.net"])

    csp_default_src = "default-src 'self' 'unsafe-inline' ${local.csp_allowed_domains}"
    csp_img_src = "img-src 'self' ${local.csp_allowed_domains}"
    csp_style_src = "style-src-elem 'self' 'unsafe-inline' ${local.csp_allowed_domains}"
    csp_script_src = "script-src 'self' 'unsafe-inline' 'unsafe-eval' ${local.csp_allowed_domains}"
    csp_frame_src = "frame-src 'self' ${local.csp_allowed_domains}"
}

resource "aws_cloudfront_response_headers_policy" "security_headers" {
    name = "security-response-header-policy-${random_id.cd_function_suffix.hex}-${var.project_name}-${var.resource_namespace}"
    comment = "Headers policy for ${var.project_name}-${var.resource_namespace}"

    cors_config {
        access_control_allow_credentials = true
        access_control_max_age_sec = 3000
        origin_override = true
        access_control_allow_headers {
            items = ["Origin", "Sec-Ch-Ua", "Sec-Ch-Ua-Mobile"]
        }
        access_control_allow_methods {
            items = ["GET", "HEAD", "OPTIONS"]
        }
        access_control_allow_origins {
            items = setunion(
                [for domain in var.website_aliases: "https://${domain}"],
                ["http://localhost:1234", "https://app.contentful.com"]
            )
        }
    }

    security_headers_config {
        content_security_policy {
            # default-src 'self' 'unsafe-inline' ben.website www.ben.website localhost:1234 app.contentful.com; img-src 'self' ben.website www.ben.website localhost:1234 app.contentful.com images.ctfassets.net; script-src 'self' 'unsafe-inline' ben.website www.ben.website localhost:1234 app.contentful.com; style-src-elem 'self' 'unsafe-inline' ben.website www.ben.website localhost:1234 app.contentful.com;
            content_security_policy = "${local.csp_default_src}; ${local.csp_img_src}; ${local.csp_script_src}; ${local.csp_style_src};"
            override = true
        }

        content_type_options {
            override = true
        }

        # frame_options {
        #     frame_option = "SAMEORIGIN"
        #     override = false
        # }

        referrer_policy {
            referrer_policy = "same-origin"
            override = true
        }

        strict_transport_security {
            access_control_max_age_sec = 31536000
            include_subdomains = true
            preload = true
            override = true
        }

        xss_protection {
            override = true
            mode_block = true
            protection = true
        }
    }
}
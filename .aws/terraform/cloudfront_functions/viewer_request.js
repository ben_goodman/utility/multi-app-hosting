function handler(event) {
    console.log('event', event)
    var request = event.request

    // case of navigating to root URL
    if (request.uri.endsWith('/')) {
        request.uri += 'index.html'
    }

    // case of navigating elsewhere in the site
    var splitUri = request.uri.split('/')
    if (splitUri[splitUri.length - 1].indexOf('.') < 0) {
        request.uri += '/index.html'
    }

    console.log('Request: ', JSON.stringify(request))

    return request
}
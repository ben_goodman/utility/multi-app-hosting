terraform {
  backend "s3" {
    bucket = "tf-states-us-east-1-bgoodman"
    key    = "s3-website/multi-app-hosting/terraform.tfstate"
    region = "us-east-1"
  }
}
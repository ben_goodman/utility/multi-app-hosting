project_name = "multi-app-hosting"
resource_namespace = "bgoodman"
hosted_zone_name = "ben.website"
website_aliases  = ["apps.ben.website", "www.apps.ben.website"]
acm_certificate_arn = "arn:aws:acm:us-east-1:757687723154:certificate/41eba475-aca9-4446-ad22-f82bf0ffca82"

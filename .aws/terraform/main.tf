resource "random_id" "cd_function_suffix" {
    byte_length = 2
}

module "website" {
    source  = "gitlab.com/ben_goodman/s3-website/aws"
    version = "2.0.0"

    org          = var.resource_namespace
    project_name = var.project_name
    aliases      = var.website_aliases
    use_cloudfront_default_certificate = false
    acm_certificate_arn = var.acm_certificate_arn

    default_cache_policy_id = aws_cloudfront_cache_policy.default.id
    default_response_headers_policy_id = aws_cloudfront_response_headers_policy.security_headers.id

    default_cloudfront_function_associations = {
        "viewer-request" = {
            function_arn = aws_cloudfront_function.viewer_request.arn
        }
    }
}

module "dns_alias" {
    source  = "gitlab.com/ben_goodman/s3-website/aws//modules/dns"
    version = "2.0.0"

    hosted_zone_name       = var.hosted_zone_name
    cloudfront_domain_name = module.website.cloudfront_default_domain
    domain_name            = var.website_aliases
}
